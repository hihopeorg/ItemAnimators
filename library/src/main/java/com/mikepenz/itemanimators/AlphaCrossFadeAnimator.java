package com.mikepenz.itemanimators;


public class AlphaCrossFadeAnimator extends BaseItemAnimator {

    public AlphaCrossFadeAnimator () {
        setDuration(200);
        setCurveType(CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                getComponent().setAlpha(0.5f+v);
            } else {
                getComponent().setAlpha(1 - v);
            }
        });
    }
}
