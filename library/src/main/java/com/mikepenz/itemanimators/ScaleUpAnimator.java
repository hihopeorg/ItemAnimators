package com.mikepenz.itemanimators;


public class ScaleUpAnimator extends BaseItemAnimator {
    public ScaleUpAnimator () {
        setDuration(500);
        setCurveType(CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                if(v>0.1){
                    getComponent().setAlpha(1);
                }
                getComponent().setScaleX(v);
                getComponent().setScaleY(v);
            } else {
                getComponent().setAlpha(1);
                getComponent().setScaleX(1 - v);
                getComponent().setScaleY(1-v);
            }
        });
    }
}
