package com.mikepenz.itemanimators;


public class ScaleXAnimator extends BaseItemAnimator {

    public ScaleXAnimator () {
        setDuration(500);
        setCurveType(CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                getComponent().setAlpha(1);
               getComponent().setScaleX(v);
            } else {
                getComponent().setAlpha(1);
                getComponent().setScaleX(1 - v);
            }
        });
    }
}
