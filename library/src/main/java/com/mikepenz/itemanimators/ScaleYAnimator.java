package com.mikepenz.itemanimators;


public class ScaleYAnimator extends BaseItemAnimator {
    public ScaleYAnimator() {
        setDuration(500);
        setCurveType(CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                getComponent().setAlpha(1);
                getComponent().setScaleY(v);
            } else {
                getComponent().setAlpha(1);
                getComponent().setScaleX(1 - v);
            }
        });
    }
}
