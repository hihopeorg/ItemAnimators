package com.mikepenz.itemanimators;


import ohos.agp.animation.Animator;

/**
 * Created by mikepenz on 08.01.16.
 */
public class DefaultAnimator extends BaseItemAnimator {
    // ADD ANIMATION METHODS
    private float positionX;
    private float positionY;


    public DefaultAnimator addAnimationPrepare() {
        setDuration(500);
        setCurveType(Animator.CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                getComponent().setAlpha(0);

            } else {

                getComponent().setAlpha(1);
            }
        });
        return this;
    }


    public DefaultAnimator addAnimation() {
        setDuration(500);
        setCurveType(Animator.CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                getComponent().setAlpha(1);

            } else {

                getComponent().setAlpha(0);
            }
        });
        return this;
    }


    public DefaultAnimator addAnimationCleanup() {
        setDuration(500);
        setCurveType(Animator.CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                getComponent().setAlpha(1);

            } else {

                getComponent().setAlpha(0);
            }
        });
        return this;
    }

    public DefaultAnimator removeAnimation() {
        setDuration(500);
        setCurveType(Animator.CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                getComponent().setAlpha(0);

            } else {
                getComponent().setAlpha(1);
            }
        });
        return this;
    }

    public DefaultAnimator removeAnimationCleanup() {
        setDuration(500);
        setCurveType(Animator.CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                getComponent().setAlpha(1);

            } else {
                getComponent().setAlpha(0);
            }
        });
        return this;
    }


    public DefaultAnimator changeOldAnimation() {

        setDuration(500);
        setCurveType(Animator.CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (positionX == 0) {
                positionX = getComponent().getContentPositionX();
            }
            if (positionY == 0) {
                positionY = getComponent().getContentPositionY();
            }

            if (type == 0) {
                getComponent().setAlpha(v);
                getComponent().setContentPositionX(positionX - (1 - v) * getComponent().getWidth());
                getComponent().setContentPositionY(positionY - (1 - v) * getComponent().getHeight());
            } else {
                getComponent().setAlpha(1 - v);
                getComponent().setContentPositionX(positionX - (v) * getComponent().getWidth());
                getComponent().setContentPositionY(positionY - (v) * getComponent().getHeight());


            }
        });

        return this;
    }


    public DefaultAnimator changeNewAnimation() {
        setDuration(500);
        setCurveType(Animator.CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {

            getComponent().setContentPosition(0, 0);
            getComponent().setAlpha(v);


        });
        return this;
    }


    public DefaultAnimator changeAnimationCleanup() {
        setDuration(500);
        setCurveType(Animator.CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            getComponent().setAlpha(v);
        });
        return this;
    }


}
