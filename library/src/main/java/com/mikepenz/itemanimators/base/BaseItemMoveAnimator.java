/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mikepenz.itemanimators.base;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

import static com.mikepenz.itemanimators.base.BaseGridAnimProvider.GRID_COLUMN;


public class BaseItemMoveAnimator {
    /**
     * 最右边的移动到左下
     *
     * @param component
     * @param type
     */
    public static AnimatorValue rightMoveToLowerLeft(Component component, int type, long duration) {

        float positionY = component.getContentPositionY();
        float positionX = component.getContentPositionX();
        AnimatorValue value = new AnimatorValue();
        value.setDuration(duration);
        value.setCurveType(Animator.CurveType.LINEAR);
        value.setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                //x向左边移动的距离
                component.setContentPositionX(positionX - v * (component.getWidth() + component.getMarginRight() + component.getMarginLeft()) * (GRID_COLUMN - 1));
                //y向下移动的距离
                component.setContentPositionY(positionY + v * component.getHeight() + component.getMarginTop() + component.getMarginBottom());

            } else {
                component.setContentPositionX(positionX + v * (component.getWidth() + component.getMarginRight()+component.getMarginLeft()) * (GRID_COLUMN - 1));
                component.setContentPositionY(positionY - v * (component.getHeight()+component.getMarginTop()+component.getMarginBottom()));
            }
        });
        return value;
    }


    /**
     * 中间位置往左下角移动
     */

     public  static  AnimatorValue centerMoveToLowerLeft(Component component, int type, long duration){
         float positionY = component.getContentPositionY();
         float positionX = component.getContentPositionX();

         AnimatorValue value = new AnimatorValue();
         value.setDuration(duration);
         value.setCurveType(Animator.CurveType.LINEAR);
         value.setValueUpdateListener((animatorValue, v) -> {
             if (type == 0) {
                 //x向左边移动的距离
                 component.setContentPositionX(positionX - v * (component.getWidth() + component.getMarginRight() + component.getMarginLeft()));
                 //y向下移动的距离
                 component.setContentPositionY(positionY + v * component.getHeight() + component.getMarginTop() + component.getMarginBottom());

             } else {
                 component.setContentPositionX(positionX + v * (component.getWidth() + component.getMarginRight()+component.getMarginLeft()));
                 component.setContentPositionY(positionY - v * (component.getHeight()+component.getMarginTop()+component.getMarginBottom()));


             }
         });
         return value;

     }


    /**
     * 最左边的往中间移动
     * 或者右边的往中间移动
     *
     * @param component
     * @param type
     * @return
     */
    public static AnimatorValue leftMoveToCenter(Component component, int type, long duration) {
        float positionX = component.getContentPositionX();
        AnimatorValue value = new AnimatorValue();
        value.setDuration(duration);
        value.setCurveType(Animator.CurveType.LINEAR);
        value.setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                //x向右边边移动的距离
                component.setContentPositionX(positionX + v * (component.getWidth() + component.getMarginRight() + component.getMarginLeft()));
            } else {
                //x 向左边移动的距离
                component.setContentPositionX(positionX - v * (component.getWidth() +component.getMarginRight() + component.getMarginLeft()));

            }
        });
        return value;

    }


    public static abstract class EndMoveStateAnimator implements Animator.StateChangedListener {

        @Override
        public void onStart(Animator animator) {

        }

        @Override
        public void onStop(Animator animator) {

        }

        @Override
        public void onCancel(Animator animator) {

        }


        @Override
        public void onPause(Animator animator) {

        }

        @Override
        public void onResume(Animator animator) {

        }
    }


}
