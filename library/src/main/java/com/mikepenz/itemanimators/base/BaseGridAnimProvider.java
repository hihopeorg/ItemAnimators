/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mikepenz.itemanimators.base;

import com.mikepenz.itemanimators.BaseItemAnimator;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class BaseGridAnimProvider<T> extends BaseItemProvider {
    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "BaseGridAnimProvider");
    private boolean isAnimator = false;
    private long addDuration = 500;
    private long removeDuration = 500;

    public List<Component> visibleList = new ArrayList<>();
    public List<T> mData;
    private Context mContext;
    private BaseItemAnimator mAnimator;

    private ListContainer mListContainer;

    public final static int GRID_COLUMN = 3;

    private  boolean isChanged=false;


   public  abstract  void  updateStatus(T t);

   public  abstract  boolean isSelected(T t);



    public BaseGridAnimProvider(List<T> data, Context context, ListContainer listContainer) {
        mData = data;
        mContext = context;
        mListContainer = listContainer;
    }

    public void setItemAnimator(BaseItemAnimator baseItemAnimator) {
        if (baseItemAnimator != null) {
            this.mAnimator = baseItemAnimator;
            isAnimator = true;

        }

    }

    public void setItemAddDuration(long duration) {
        addDuration = duration;
    }


    public void setRemoveDuration(long duration) {
        removeDuration = duration;

    }

    public long getAddDuration() {
        return addDuration;
    }


    /**
     * 添加数据
     *
     * @param position
     * @param t
     */
    public void addItem(int position, T t) {
        isChanged=true;
        int visibleIndexCount = mListContainer.getVisibleIndexCount();
        int count = visibleIndexCount - position;
        NotifyDataEventHandler handler = new NotifyDataEventHandler(t, 0, position, count, EventRunner.getMainEventRunner());
        if (count == 0) {
            mData.add(position, t);
            notifyDataSetItemInserted(position);
        } else {
            for (int i = position; i < visibleIndexCount; i++) {
                int visibleIndex = mListContainer.getItemPosByVisibleIndex(i);  //获取可见的区域的下标
                Component component = mListContainer.getComponentAt(visibleIndex);//根据下标获取item
                AnimatorValue animatorValue;
                if (i % 3 != 2) {
                    animatorValue = BaseItemMoveAnimator.leftMoveToCenter(component, 0, getAddDuration());
                } else {
                    animatorValue = BaseItemMoveAnimator.rightMoveToLowerLeft(component, 0, getAddDuration());
                }
                animatorValue.setStateChangedListener(new BaseItemMoveAnimator.EndMoveStateAnimator() {
                    @Override
                    public void onEnd(Animator animator) {
                        handler.sendEvent(100);
                    }
                });
                animatorValue.start();


            }

        }

    }


    public void removeItem(int position) {
        isChanged=true;
        int visibleIndexCount = mListContainer.getVisibleIndexCount();
        int count = mListContainer.getVisibleIndexCount();
        NotifyDataEventHandler handler = new NotifyDataEventHandler(null, 1, position, count, EventRunner.getMainEventRunner());

        if (count > 1) {
            for (int i = position; i < visibleIndexCount; i++) {
                int visibleIndex = mListContainer.getItemPosByVisibleIndex(i);  //获取可见的区域的下标
                Component component = mListContainer.getComponentAt(visibleIndex);//根据下标获取item
                AnimatorValue animatorValue;
                if (i % 3 == 0) {
                    animatorValue = BaseItemMoveAnimator.rightMoveToLowerLeft(component, 1, getAddDuration());
                } else {
                    animatorValue = BaseItemMoveAnimator.leftMoveToCenter(component, 1, getAddDuration());
                }
                animatorValue.setStateChangedListener(new BaseItemMoveAnimator.EndMoveStateAnimator() {
                    @Override
                    public void onEnd(Animator animator) {
                        handler.sendEvent(100);
                    }
                });
                animatorValue.start();


            }
        }


        mData.remove(position);
        notifyDataSetItemRemoved(position);
    }


    public void change(int position) {
        isChanged=false;
        int visibleIndexCount = mListContainer.getVisibleIndexCount();
        if (visibleIndexCount < 4) {
            return;
        }
        int count = mListContainer.getVisibleIndexCount();
        NotifyDataEventHandler handler = new NotifyDataEventHandler(null, 1, position, 3, EventRunner.getMainEventRunner());

        mData.add(position + 3, mData.get(position));
        if (count > 1) {
            AnimatorValue animatorValue0 = null;
            AnimatorValue animatorValue1 = null;
            AnimatorValue animatorValue2 = null;


            Component component = mListContainer.getComponentAt(position);//根据下标获取item
            animatorValue0 = BaseItemMoveAnimator.centerMoveToLowerLeft(component, 0, getAddDuration());

            animatorValue0.setStateChangedListener(new BaseItemMoveAnimator.EndMoveStateAnimator() {
                @Override
                public void onEnd(Animator animator) {
                    handler.sendEvent(200);
                }
            });
            animatorValue0.start();

            Component component1 = mListContainer.getComponentAt(position + 1);//根据下标获取item
            animatorValue1 = BaseItemMoveAnimator.leftMoveToCenter(component1, 1, getAddDuration());

            animatorValue1.setStateChangedListener(new BaseItemMoveAnimator.EndMoveStateAnimator() {
                @Override
                public void onEnd(Animator animator) {
                    handler.sendEvent(200);
                }
            });
            animatorValue1.start();
            Component component2 = mListContainer.getComponentAt(position + 2);//根据下标获取item
            animatorValue2 = BaseItemMoveAnimator.rightMoveToLowerLeft(component2, 1, getAddDuration());
            animatorValue2.setStateChangedListener(new BaseItemMoveAnimator.EndMoveStateAnimator() {
                @Override
                public void onEnd(Animator animator) {
                    handler.sendEvent(200);
                }
            });
            animatorValue2.start();

        }


    }


    /**
     * 更新数据
     */
    public class NotifyDataEventHandler extends EventHandler {
        private int count;
        private int total;
        private int type;
        private int position;
        public T t;

        public NotifyDataEventHandler(T t, int type, int position, int total, EventRunner runner) throws IllegalArgumentException {
            super(runner);
            this.total = total;
            this.type = type;
            this.position = position;
            this.t = t;
        }


        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event == null) {
                return;
            }
            count++;
            int eventId = event.eventId;
            if (eventId == 100) {
                if (total == count) {
                    if (type == 0) {
                        mData.add(position, t);
                        notifyDataSetItemInserted(position);
                    } else {
                        mData.remove(position);
                        notifyDataSetItemRemoved(position);
                    }
                }
            } else if (eventId == 200) {
                if (total == count) {
                    mData.remove(position);
                    notifyDataChanged();
                }
            }
        }
    }


    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();

    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ComponentHolder componentHolder;
        if (component == null) {
            componentHolder = new ComponentHolder();
            component = LayoutScatter.getInstance(mContext).parse(getLayoutResId(), null, false);
            intLayout(component);
            //将控件与 ComponentHolder 绑定
            int[] componentById = intComponentById();
            for (int id : componentById) {
                componentHolder.bindComponentById(component, id);
            }
            component.setTag(componentHolder);

        } else {
            componentHolder = (ComponentHolder) component.getTag();
        }


        Component finalComponent = component;
        component.setLongClickedListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                 if(!isSelected( mData.get(i))){
                     finalComponent.setAlpha(0.5f);
                 }else {

                     finalComponent.setAlpha(1f);
                 }

                updateStatus(mData.get(i));

            }
        });



        if (isAnimator&&isChanged) {
            mAnimator.setComponent(component);
            mAnimator.setType(0);
            mAnimator.setStateChangedListener(new BaseItemMoveAnimator.EndMoveStateAnimator() {
                @Override
                public void onEnd(Animator animator) {
                    finalComponent.setAlpha(1);
                }
            });
            mAnimator.start();
        }
        visibleList.add(i, component);
        bindData(mContext, componentHolder, mData.get(i));
        return component;
    }

    protected abstract int getLayoutResId();


    /**
     * 将控件与 ComponentHolder 绑定
     */
    protected abstract int[] intComponentById();

    /**
     * 设置item 边距
     *
     * @param component
     */
    protected abstract void intLayout(Component component);

    /**
     * @param holder
     * @param t
     */
    protected abstract void bindData(Context context, ComponentHolder holder, T t);


    public class ComponentHolder {

        private HashMap<Integer, Component> viewSparseArray;

        ComponentHolder() {
            viewSparseArray = new HashMap<>();
        }

        /**
         * 通过 id获取 Component
         */
        public <E extends Component> E findComponentById(int id) {
            return (E) viewSparseArray.get(id);
        }


        public void bindComponentById(Component view, int id) {
            viewSparseArray.put(id, view.findComponentById(id));
        }
    }


}
