package com.mikepenz.itemanimators;

public class SlideLeftAlphaAnimator extends BaseItemAnimator {

    public SlideLeftAlphaAnimator() {
        setDuration(500);
        setCurveType(CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                float width = getComponent().getWidth();
                float marginRight = getComponent().getMarginRight();
                float marginLeft = getComponent().getMarginLeft();
                float offsetX = width * 0.25f;
                getComponent().setAlpha(1);
                getComponent().setContentPositionX(offsetX + (width + marginRight + marginRight + marginLeft - offsetX) * v);
            }
        });
    }
}
