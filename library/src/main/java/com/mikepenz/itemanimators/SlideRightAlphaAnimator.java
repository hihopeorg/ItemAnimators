package com.mikepenz.itemanimators;

public class SlideRightAlphaAnimator extends BaseItemAnimator {

    public SlideRightAlphaAnimator() {
        setDuration(500);
        setCurveType(CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (type == 0) {
                float width = getComponent().getWidth();
                float marginRight = getComponent().getMarginRight();
                float marginLeft = getComponent().getMarginLeft();
                float offsetX = width * 1.75f;
                getComponent().setAlpha(1);
                getComponent().setContentPositionX(offsetX - width * 0.75f*v+marginLeft+marginRight+marginLeft);
            }
        });
    }

}
