package com.mikepenz.itemanimators;

public class SlideUpAlphaAnimator extends BaseItemAnimator {
    private float positionY;

    public SlideUpAlphaAnimator() {
        setDuration(500);
        setCurveType(CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (positionY == 0) {
                positionY = getComponent().getContentPositionY();
            }
            if (type == 0) {
                getComponent().setAlpha(1);
                getComponent().setContentPositionY(positionY + (1 - v) * getComponent().getHeight());
            } else {
                getComponent().setContentPositionY(positionY + v * getComponent().getHeight());
                getComponent().setAlpha(1 - v);
            }
        });
    }

}
