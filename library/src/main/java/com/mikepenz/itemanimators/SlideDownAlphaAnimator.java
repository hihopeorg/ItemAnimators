package com.mikepenz.itemanimators;


public class SlideDownAlphaAnimator extends BaseItemAnimator {
    private float positionY;

    public SlideDownAlphaAnimator() {
        setDuration(500);
        setCurveType(CurveType.LINEAR);
        setValueUpdateListener((animatorValue, v) -> {
            if (positionY == 0) {
                positionY = getComponent().getContentPositionY();
            }
            if (type == 0) {
                getComponent().setContentPositionY(positionY - (1 - v) * getComponent().getHeight());
                getComponent().setAlpha(v);
            } else {
                getComponent().setContentPositionY(positionY - (v) * getComponent().getHeight());
                getComponent().setAlpha(1 - v);
            }
        });
    }


}
