package com.mikepenz.itemanimators;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public  class BaseItemAnimator extends AnimatorValue {
    Component component;
    public int type;

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    public BaseItemAnimator copy() {
        return new BaseItemAnimator();
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
