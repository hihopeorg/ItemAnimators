package com.mikepenz.itemanimators.app.dummy;


import com.mikepenz.itemanimators.app.ResourceTable;
import com.mikepenz.itemanimators.app.items.ImageItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by mikepenz on 08.01.16.
 */
public class ImageDummyData {

    public static ImageItem getDummyItem() {
        int ran = new Random().nextInt(3);
        if (ran == 0) {

            return new ImageItem().withName("NEW").withDescription("Newly added item").withImage(ResourceTable.Media_yang_zhuo_yong_cuo_tibet_china_63);
        } else if (ran == 1) {
            return new ImageItem().withName("NEW").withDescription("Newly added item").withImage(ResourceTable.Media_yellowstone_united_states_17);
        } else {
            return new ImageItem().withName("NEW").withDescription("Newly added item").withImage(ResourceTable.Media_victoria_australia_31);
        }
    }

    public static List<ImageItem> getImages() {
        return toList(
                new ImageItem().withName("Yang Zhuo Yong Cuo, Tibet China").withDescription("#100063").withImage(ResourceTable.Media_yang_zhuo_yong_cuo_tibet_china_63),
                new ImageItem().withName("Yellowstone United States").withDescription("#100017").withImage(ResourceTable.Media_yellowstone_united_states_17),

                new ImageItem().withName("Victoria Australia").withDescription("#100031").withImage(ResourceTable.Media_victoria_australia_31),
                new ImageItem().withName("Valencia Spain").withDescription("#100082").withImage(ResourceTable.Media_valencia_spain_82),

                new ImageItem().withName("Xigaze, Tibet China").withDescription("#100030").withImage(ResourceTable.Media_xigaze_tibet_china_30),

                new ImageItem().withName("Utah United States").withDescription("#100096").withImage(ResourceTable.Media_utah_united_states_96),

                new ImageItem().withName("Utah United States").withDescription("#100015").withImage(ResourceTable.Media_utah_united_states_15),

                new ImageItem().withName("Utah United States").withDescription("#100088").withImage(ResourceTable.Media_utah_united_states_88),

                new ImageItem().withName("Umm Al Quwain United Arab Emirates").withDescription("#100013").withImage(ResourceTable.Media_umm_al_quwain_united_arab_emirates_13),

                new ImageItem().withName("Texas United States").withDescription("#100026").withImage(ResourceTable.Media_texas_united_states_26),

                new ImageItem().withName("Siuslaw National Forest United States").withDescription("#100092").withImage(ResourceTable.Media_siuslaw_national_forest_united_states_92),

                new ImageItem().withName("The Minquiers Channel Islands").withDescription("#100069").withImage(ResourceTable.Media_the_minquiers_channel_islands_69),

                new ImageItem().withName("Texas United States").withDescription("#100084").withImage(ResourceTable.Media_texas_united_states_84),

                new ImageItem().withName("Tabuaeran Kiribati").withDescription("#100050").withImage(ResourceTable.Media_tabuaeran_kiribati_50),

                new ImageItem().withName("Stanislaus River United States").withDescription("#100061").withImage(ResourceTable.Media_stanislaus_river_united_states_61),

                new ImageItem().withName("Salinas Grandes Argentina").withDescription("#100025").withImage(ResourceTable.Media_salinas_grandes_argentina_25),

                new ImageItem().withName("Shadegan Refuge Iran").withDescription("#100012").withImage(ResourceTable.Media_shadegan_refuge_iran_12),

                new ImageItem().withName("San Pedro De Atacama Chile").withDescription("#100043").withImage(ResourceTable.Media_san_pedro_de_atacama_chile_43),

                new ImageItem().withName("Ragged Island The Bahamas").withDescription("#100064").withImage(ResourceTable.Media_ragged_island_the_bahamas_64),

                new ImageItem().withName("Qinghai Lake China").withDescription("#100080").withImage(ResourceTable.Media_qinghai_lake_china_80),

                new ImageItem().withName("Qesm Al Wahat Ad Dakhlah Egypt").withDescription("#100056").withImage(ResourceTable.Media_qesm_al_wahat_ad_dakhlah_egypt_56),

                new ImageItem().withName("Riedstadt Germany").withDescription("#100042").withImage(ResourceTable.Media_riedstadt_germany_42),

                new ImageItem().withName("Redwood City United States").withDescription("#100048").withImage(ResourceTable.Media_redwood_city_united_states_48),

                new ImageItem().withName("Nyingchi, Tibet China").withDescription("#100098").withImage(ResourceTable.Media_nyingchi_tibet_china_98),

                new ImageItem().withName("Ngari, Tibet China").withDescription("#100057").withImage(ResourceTable.Media_ngari_tibet_china_57),

                new ImageItem().withName("Pozoantiguo Spain").withDescription("#100099").withImage(ResourceTable.Media_pozoantiguo_spain_99),

                new ImageItem().withName("Ningaloo Australia").withDescription("#100073").withImage(ResourceTable.Media_ningaloo_australia_73),

                new ImageItem().withName("Niederzier Germany").withDescription("#100079").withImage(ResourceTable.Media_niederzier_germany_79),

                new ImageItem().withName("Olympic Dam Australia").withDescription("#100065").withImage(ResourceTable.Media_olympic_dam_australia_65),


                new ImageItem().withName("Peedamulla Australia").withDescription("#100040").withImage(ResourceTable.Media_peedamulla_australia_40),

                new ImageItem().withName("Nevado Tres Cruces Park Chile").withDescription("#100089").withImage(ResourceTable.Media_nevado_tres_cruces_park_chile_89)
        );
    }



    private static List<ImageItem> toList(ImageItem... imageItems) {
        ArrayList<ImageItem> items = new ArrayList<>();
        for (ImageItem imageItem : imageItems) {
            items.add(imageItem);
        }
        return items;
    }
}
