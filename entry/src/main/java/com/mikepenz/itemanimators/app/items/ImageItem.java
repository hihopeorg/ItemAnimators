package com.mikepenz.itemanimators.app.items;


public class ImageItem {



    public int mImageUrl;
    public String mName;
    public String mDescription;
    public boolean  isSelected=false;

    public ImageItem withImage(int imageUrl) {
        this.mImageUrl = imageUrl;
        return this;
    }

    public ImageItem withName(String name) {
        this.mName = name;
        return this;
    }

    public ImageItem withDescription(String description) {
        this.mDescription = description;
        return this;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }
}
