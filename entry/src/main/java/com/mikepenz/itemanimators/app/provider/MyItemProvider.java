/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mikepenz.itemanimators.app.provider;

import com.mikepenz.itemanimators.app.ResourceTable;
import com.mikepenz.itemanimators.app.items.ImageItem;
import com.mikepenz.itemanimators.base.BaseGridAnimProvider;
import ohos.agp.components.*;
import ohos.app.Context;
import uitls.AppUtils;

import java.util.List;

public class MyItemProvider extends BaseGridAnimProvider<ImageItem> {
    private int ids[];

    @Override
    public void updateStatus(ImageItem imageItem) {
        imageItem.setSelected(!imageItem.isSelected);
    }

    @Override
    public boolean isSelected(ImageItem imageItem) {
        return imageItem.isSelected;
    }

    public MyItemProvider(List<ImageItem> data, Context context, ListContainer listContainer) {
        super(data, context, listContainer);
    }

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_item_layout;
    }

    @Override
    protected int[] intComponentById() {
        ids = new int[]{ResourceTable.Id_itemImage, ResourceTable.Id_nameText, ResourceTable.Id_descriptionText};
        return ids;
    }

    @Override
    protected void intLayout(Component component) {
        component.setWidth(AppUtils.getItemWidthByScreen(component.getContext(), 3));
        component.setMarginsLeftAndRight(10, 10);
        component.setMarginsTopAndBottom(10, 10);
    }

    @Override
    protected void bindData(Context context, ComponentHolder holder, ImageItem imageItem) {
        Image itemImage = holder.findComponentById(ids[0]);
        Text name = holder.findComponentById(ids[1]);
        Text descriptionText = holder.findComponentById(ids[2]);
        itemImage.setPixelMap(imageItem.mImageUrl);
        name.setText(imageItem.mName);
        descriptionText.setText(imageItem.mDescription);

    }
}
