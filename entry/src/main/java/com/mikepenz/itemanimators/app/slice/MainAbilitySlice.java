package com.mikepenz.itemanimators.app.slice;

import com.mikepenz.itemanimators.*;
import com.mikepenz.itemanimators.app.ResourceTable;
import com.mikepenz.itemanimators.app.dummy.ImageDummyData;
import com.mikepenz.itemanimators.app.items.ImageItem;
import com.mikepenz.itemanimators.app.provider.MyItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ListDialog;
import ohos.agp.window.service.WindowManager;
import uitls.AppUtils;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    private MyItemProvider mProvider;
    private List<ImageItem> mData;
    private ListContainer listContainer;
    private Text textType;
    private ListDialog listDialog;

    public ListDialog getListDialog() {
        return listDialog;
    }

    public MyItemProvider getProvider() {
        return mProvider;
    }

    enum Type {
        CrossFade(new AlphaCrossFadeAnimator()),
        FadeIn(new AlphaInAnimator()),
        ScaleUp(new ScaleUpAnimator()),
        ScaleX(new ScaleXAnimator()),
        ScaleY(new ScaleYAnimator()),
        SlideDownAlpha(new SlideDownAlphaAnimator()),
        SlideLeftAlpha(new SlideLeftAlphaAnimator()),
        SlideRightAlpha(new SlideRightAlphaAnimator()),
        SlideUpAlpha(new SlideUpAlphaAnimator()
        );

        private BaseItemAnimator mAnimator;

        Type(BaseItemAnimator animator) {
            mAnimator = animator;
        }

        public BaseItemAnimator getAnimator() {
            return mAnimator;
        }
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        listContainer.setWidth(AppUtils.getScreenInfo(this).getPointXToInt());
        mData = ImageDummyData.getImages();
        mProvider = new MyItemProvider(mData, this, listContainer);
        listContainer.setItemProvider(mProvider);
        mProvider.setItemAddDuration(200);
        mProvider.setRemoveDuration(200);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(3);
        listContainer.setLayoutManager(tableLayoutManager);
        mProvider.setItemAnimator(Type.values()[0].getAnimator());

        initActionBar();


    }


    private void initActionBar() {
        String[] items = new String[Type.values().length];
        listDialog = new ListDialog(MainAbilitySlice.this);
        listDialog.setAlignment(LayoutAlignment.START | LayoutAlignment.TOP);
        listDialog.setSize(400, MATCH_CONTENT);
        listDialog.setAutoClosable(true);
        for (int i = 0; i < items.length; i++) {
            items[i] = Type.values()[i].name();
        }
        listDialog.setItems(items);

        listDialog.setOnSingleSelectListener((iDialog, index) -> {
            textType.setText(items[index]);

            mProvider.setItemAnimator(Type.values()[index].getAnimator());

            iDialog.hide();
        });

        textType = (Text) findComponentById(ResourceTable.Id_typeText);
        textType.setClickedListener(component -> listDialog.show());


        findComponentById(ResourceTable.Id_addImage).setClickedListener(component -> {
            int visibleIndex = listContainer.getItemPosByVisibleIndex(0);
            mProvider.addItem(visibleIndex + 1, ImageDummyData.getDummyItem());
        });
        findComponentById(ResourceTable.Id_deleteImage).setClickedListener(component -> {
            int visibleIndex = listContainer.getItemPosByVisibleIndex(0);
            mProvider.removeItem(visibleIndex + 1);
        });

        findComponentById(ResourceTable.Id_download).setClickedListener(component -> {
            int visibleIndex = listContainer.getItemPosByVisibleIndex(0);
            mProvider.change(visibleIndex + 1);

        });


    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


}
