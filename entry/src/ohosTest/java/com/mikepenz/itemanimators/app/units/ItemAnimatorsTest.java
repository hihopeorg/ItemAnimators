package com.mikepenz.itemanimators.app.units;

import com.mikepenz.itemanimators.app.dummy.ImageDummyData;
import com.mikepenz.itemanimators.app.items.ImageItem;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.utils.Point;
import org.junit.Test;
import uitls.AppUtils;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class ItemAnimatorsTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();



    @Test
    public void getDummyItem() {
        ImageItem imageItem = ImageDummyData.getDummyItem();
        assertTrue("It's right", !imageItem.mName.isEmpty());
    }


    @Test
    public void getImages() {
        List<ImageItem> list = ImageDummyData.getImages();
        assertTrue("It's right", list.size() > 0);

    }

    @Test
    public void getScreenInfo() {
        Point point = AppUtils.getScreenInfo(sAbilityDelegator.getAppContext());
        float pointX = point.getPointX();
        assertTrue("It's right", pointX > 0);
    }

    @Test
    public  void getItemWidthByScreen(){
        int itemWidthByScreen = AppUtils.getItemWidthByScreen(sAbilityDelegator.getAppContext(),4);
        assertTrue("It's right", itemWidthByScreen > 0);

    }




}
