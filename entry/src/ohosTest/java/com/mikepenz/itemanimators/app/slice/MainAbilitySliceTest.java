package com.mikepenz.itemanimators.app.slice;

import com.mikepenz.itemanimators.*;
import com.mikepenz.itemanimators.app.MainAbility;
import com.mikepenz.itemanimators.app.ResourceTable;
import com.mikepenz.itemanimators.app.items.ImageItem;
import com.mikepenz.itemanimators.app.provider.MyItemProvider;
import com.mikepenz.itemanimators.app.uitls.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Button;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.window.dialog.ListDialog;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class MainAbilitySliceTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Before
    public void startUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        EventHelper.clearAbilities();
    }


    @Test
    public void addItem() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        int count = mainAbilitySlice.getProvider().getCount();
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);
    }

    @Test
    public void removeItem() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        int count = mainAbilitySlice.getProvider().getCount();
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_deleteImage);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);
    }

    @Test
    public void change() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image changeImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_download);
        List<ImageItem> imageItemList = mainAbilitySlice.getProvider().mData;
        ImageItem imageItem = imageItemList.get(1);
        String name = imageItem.mName;
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, changeImage);
        sleep(3000);
        List<ImageItem> imageItemList2 = mainAbilitySlice.getProvider().mData;
        ImageItem imageItem2 = imageItemList.get(1);
        assertTrue("It's right", !name.equals(imageItem2.mName));
        sAbilityDelegator.stopAbility(mainAbility);


    }


    @Test
    public void testAlphaCrossFadeAnimator() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        MyItemProvider myItemProvider = mainAbilitySlice.getProvider();
        int count = mainAbilitySlice.getProvider().getCount();
        AlphaCrossFadeAnimator alphaCrossFadeAnimator = new AlphaCrossFadeAnimator();
        myItemProvider.setItemAnimator(alphaCrossFadeAnimator);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);

    }

    @Test
    public void testAlphaInAnimator() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        MyItemProvider myItemProvider = mainAbilitySlice.getProvider();
        int count = mainAbilitySlice.getProvider().getCount();
        AlphaInAnimator alphaInAnimator = new AlphaInAnimator();
        myItemProvider.setItemAnimator(alphaInAnimator);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);

    }





    @Test
    public void testScaleUpAnimator() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        MyItemProvider myItemProvider = mainAbilitySlice.getProvider();
        int count = mainAbilitySlice.getProvider().getCount();
        ScaleUpAnimator scaleUpAnimator = new ScaleUpAnimator();
        myItemProvider.setItemAnimator(scaleUpAnimator);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);

    }

    @Test
    public  void testScaleXAnimator(){
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        MyItemProvider myItemProvider = mainAbilitySlice.getProvider();
        int count = mainAbilitySlice.getProvider().getCount();
        ScaleXAnimator scaleXAnimator = new ScaleXAnimator();
        myItemProvider.setItemAnimator(scaleXAnimator);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);

    }


    @Test
    public  void testScaleYAnimator(){
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        MyItemProvider myItemProvider = mainAbilitySlice.getProvider();
        int count = mainAbilitySlice.getProvider().getCount();
        ScaleYAnimator scaleYAnimator = new ScaleYAnimator();
        myItemProvider.setItemAnimator(scaleYAnimator);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);

    }


    @Test
    public void testSlideDownAlphaAnimator(){

        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        MyItemProvider myItemProvider = mainAbilitySlice.getProvider();
        int count = mainAbilitySlice.getProvider().getCount();
        SlideDownAlphaAnimator slideDownAlphaAnimator = new SlideDownAlphaAnimator();
        myItemProvider.setItemAnimator(slideDownAlphaAnimator);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);

    }

    @Test
    public  void  testSlideLeftAlphaAnimator(){
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        MyItemProvider myItemProvider = mainAbilitySlice.getProvider();
        int count = mainAbilitySlice.getProvider().getCount();
        SlideLeftAlphaAnimator slideLeftAlphaAnimator = new SlideLeftAlphaAnimator();
        myItemProvider.setItemAnimator(slideLeftAlphaAnimator);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);


    }



    @Test
    public  void testSlideRightAlphaAnimator(){
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        MyItemProvider myItemProvider = mainAbilitySlice.getProvider();
        int count = mainAbilitySlice.getProvider().getCount();
        SlideRightAlphaAnimator slideRightAlphaAnimator = new SlideRightAlphaAnimator();
        myItemProvider.setItemAnimator(slideRightAlphaAnimator);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);
    }

    @Test
    public  void testSlideUpAlphaAnimator(){
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Image addImage = (Image) mainAbilitySlice.findComponentById(ResourceTable.Id_addImage);
        MyItemProvider myItemProvider = mainAbilitySlice.getProvider();
        int count = mainAbilitySlice.getProvider().getCount();
        SlideUpAlphaAnimator slideUpAlphaAnimator = new SlideUpAlphaAnimator();
        myItemProvider.setItemAnimator(slideUpAlphaAnimator);
        sleep(1000);
        EventHelper.triggerClickEvent(mainAbility, addImage);
        sleep(3000);
        assertTrue("It's right", count != mainAbilitySlice.getProvider().getCount());
        sAbilityDelegator.stopAbility(mainAbility);



    }





    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}