## **ItemAnimators**
本项目是基于开源项目 ItemAnimators 进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/mikepenz/ItemAnimators ）追踪到原项目版本

#### 项目介绍

* 项目名称 ：ItemAnimators 是一个为ListContainer提供了一个巨大的预创建动画的集合
* 所属系列：ohos的第三方组件适配移植
* 功能：底部弹框展示。
* 项目移植状态：完成
* 调用差异：无
* 项目作者和维护人：hihope
* 联系方式：hihope@hoperun.com
* 原项目Doc地址：https://github.com/mikepenz/ItemAnimators
* 原项目基线版本：v1.1.0  shal:e8de6283fb80e34c72e8fac04ecb3a12b80fc674
* 编程语言：java
* 外部库依赖：无

#### 效果展示图
![](./screenshots/itemAnimators.gif)


#### 安装教程
方法(一)
1. 下载ItemAnimators组件源码编译依赖包ItemAnimators.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
方法(二)

1.在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2.在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.mikepenz.itemanimators.ohos:itemAnimators:1.0.0'
}

```
#### 使用说明
(1) 设置布局 ability_main.xml
```
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="vertical">
    <Component
        ohos:height="40vp"
        ohos:background_element="#66EEFF41"
        ohos:width="match_parent"/>

    <DirectionalLayout
        ohos:height="55vp"
        ohos:width="match_parent"


        ohos:alignment="vertical_center"
        ohos:background_element="#eeff41"
        ohos:orientation="horizontal">

        <Text
            ohos:id="$+id:typeText"
            ohos:height="match_content"
            ohos:width="150vp"
            ohos:bubble_right_height="10vp"
            ohos:bubble_right_width="10vp"
            ohos:element_right="$media:arrow"
            ohos:left_margin="15vp"
            ohos:right_padding="10vp"
            ohos:multiple_lines="true"
            ohos:text="CrossFade"
            ohos:text_size="12vp"
            />


        <Image
            ohos:id="$+id:deleteImage"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:background_element="$media:delete"
            ohos:left_margin="10vp"
            ohos:padding="13vp"
            />

        <Image
            ohos:id="$+id:addImage"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:background_element="$media:add"
            ohos:left_margin="25vp"
            ohos:padding="15vp"
            />


        <Image
            ohos:id="$+id:download"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:background_element="$media:download"
            ohos:left_margin="30vp"
            ohos:padding="13vp"/>


    </DirectionalLayout>

    <ListContainer
        ohos:id="$+id:listContainer"
        ohos:height="match_parent"
        ohos:width="match_content"/>


</DirectionalLayout>


```
(2)设置绑定加载布局

```
public class MainAbilitySlice extends AbilitySlice {
    private MyItemProvider mProvider;
    private List<ImageItem> mData;
    private ListContainer listContainer;
    private Text textType;


    enum Type {
        CrossFade(new AlphaCrossFadeAnimator()),
        FadeIn(new AlphaInAnimator()),
        ScaleUp(new ScaleUpAnimator()),
        ScaleX(new ScaleXAnimator()),
        ScaleY(new ScaleYAnimator()),
        SlideDownAlpha(new SlideDownAlphaAnimator()),
        SlideLeftAlpha(new SlideLeftAlphaAnimator()),
        SlideRightAlpha(new SlideRightAlphaAnimator()),
        SlideUpAlpha(new SlideUpAlphaAnimator()
        );

        private BaseItemAnimator mAnimator;

        Type(BaseItemAnimator animator) {
            mAnimator = animator;
        }

        public BaseItemAnimator getAnimator() {
            return mAnimator;
        }
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        listContainer.setWidth(AppUtils.getScreenInfo(this).getPointXToInt());
        mData = ImageDummyData.getImages();
        mProvider = new MyItemProvider(mData, this, listContainer);
        listContainer.setItemProvider(mProvider);
        mProvider.setItemAddDuration(200);
        mProvider.setRemoveDuration(200);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(3);
        listContainer.setLayoutManager(tableLayoutManager);
        mProvider.setItemAnimator(Type.values()[0].getAnimator());

        initActionBar();


    }


    private void initActionBar() {
        String[] items = new String[Type.values().length];
        ListDialog listDialog = new ListDialog(MainAbilitySlice.this);
        listDialog.setAlignment(LayoutAlignment.START | LayoutAlignment.TOP);
        listDialog.setSize(400, MATCH_CONTENT);
        listDialog.setAutoClosable(true);
        for (int i = 0; i < items.length; i++) {
            items[i] = Type.values()[i].name();
        }
        listDialog.setItems(items);

        listDialog.setOnSingleSelectListener((iDialog, index) -> {
            textType.setText(items[index]);

            mProvider.setItemAnimator(Type.values()[index].getAnimator());

            iDialog.hide();
        });

        textType = (Text) findComponentById(ResourceTable.Id_typeText);
        textType.setClickedListener(component -> listDialog.show());


        findComponentById(ResourceTable.Id_addImage).setClickedListener(component -> {
            int visibleIndex = listContainer.getItemPosByVisibleIndex(0);
            mProvider.addItem(visibleIndex + 1, ImageDummyData.getDummyItem());
        });
        findComponentById(ResourceTable.Id_deleteImage).setClickedListener(component -> {
            int visibleIndex = listContainer.getItemPosByVisibleIndex(0);
            mProvider.removeItem(visibleIndex + 1);
        });

        findComponentById(ResourceTable.Id_download).setClickedListener(component -> {
            int visibleIndex = listContainer.getItemPosByVisibleIndex(0);
            mProvider.change(visibleIndex + 1);

        });


    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


}


```


#### 版本迭代
v1.0.0
#### 实现功能
* 支持添加，删除，交换item动画效果

#### 版权和许可信息
```
/*
 *Copyright 2018 Mike Penz
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 */
```











